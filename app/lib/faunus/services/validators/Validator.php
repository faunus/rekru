<?php namespace faunus\services\validators;
 
abstract class Validator {
 
  protected $input;
 
  protected $errors;
 
  public function __construct($input = NULL)
  {
    $this->input = $input ?: \Input::all();
  }
 
  public function passes()
  {
    $mensajes = array(
      'required'  => 'Este Campo es Necesario',
      'email'     => 'El Correo es Inválido ):',
      'confirmed' => 'Contraseña no coincide',
    );

    $validation = \Validator::make($this->input, static::$rules, $mensajes);
 
    if($validation->passes()) return true;
     
    $this->errors = $validation->messages();
 
    return false;
  }
 
  public function getErrors()
  {
    return $this->errors;
  }
 
}