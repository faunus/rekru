<?php namespace faunus\services\validators;
 
class Mail extends Validator {
 
  /**
   * Validation rules
   */
  public static $rules = array(
    'email' => 'required|email',
  );
 
}