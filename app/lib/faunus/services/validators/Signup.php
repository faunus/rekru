<?php namespace faunus\services\validators;

class Signup extends Validator {
    
  /**
   * Validation rules
   */
  public static $rules = array(
    'name' => 'required',
    'email' => 'required|email',
    'enterprise' => 'required',
    'phone' => 'required',
    );
  
}