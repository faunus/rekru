<?php namespace faunus\mailers;

class UserMailer extends Mailer {

    public function signUp($user)
    {
        $subject = 'Rekru | Te avisamos cuando estemos listos';
        $view = 'emails/user/sign-up';

        $this->sendTo($user, $subject, $view);
    }

    public function fake($user, $subject, $view)
    {
        $this->sendToWithAttachment($user, $subject, $view);
    }

}