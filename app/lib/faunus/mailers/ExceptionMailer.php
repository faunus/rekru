<?php namespace faunus\mailers;

class ExceptionMailer extends Mailer {

    public function exception($message)
    {
    	$user = Config::get('_site.admin')->email;
        $subject = 'Exception en: ' . Config::get('_site.name');
        $view = 'emails/exception/details';
        $data = ['message' => $message];

        $this->sendTo($user, $subject, $view, $data);
    }

}