<?php namespace faunus\mailers;

use Mail;

abstract class Mailer {

    public function sendTo($user, $subject, $view, $data = [])
    {
        Mail::queue($view, $data, function($message) use ($user, $subject)
        {
            $message->to($user->email)->subject($subject);
        });
    }

    public function sendToWithAttachment($user, $subject, $view, $data = [])
    {
    	$data = [
            'username' => $user->username,
        ];
    	
        Mail::queue($view, $data, function($message) use ($user, $subject)
        {
            $message->to($user->email)->subject($subject);
            $message->attach($user->attach);
        });
    }
}