<?php namespace faunus\handlers;

use faunus\mailers\UserMailer;
use Illuminate\Events\Dispatcher;

class UserEventHandler {

    /**
     * @var UserMailer
     */
    protected $mailer;

    /**
     * @param UserMailer $mailer
     */
    function __construct(UserMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Send sign up welcome email to user
     *
     * @param $user
     */
    public function onSignUp($user)
    {
        $this->mailer->signUp($user);
    }

    public function onFake($mail, $subject, $view)
    {
        $this->mailer->fake($mail, $subject, $view);
    }

    /**
     * Register subscriptions
     *
     * @param $events
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen('user.signUp', 'faunus\handlers\UserEventHandler@onSignUp');
        $events->listen('user.send-fake-data', 'faunus\handlers\UserEventHandler@onFake');
    }
}