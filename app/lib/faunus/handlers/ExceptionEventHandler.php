<?php namespace faunus\handlers;

use faunus\mailers\ExceptionMailer;
use Illuminate\Events\Dispatcher;

class ExceptionEventHandler {

    /**
     * @var ExceptionMailer
     */
    protected $mailer;

    /**
     * @param ExceptionMailer $mailer
     */
    function __construct(ExceptionMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Send exception email to admin
     *
     * @param $user
     */
    public function onException($message)
    {
        $this->mailer->exception($message);
    }

    /**
     * Register subscriptions
     *
     * @param $events
     */
    public function subscribe(Dispatcher $events)
    {
        $events->listen('exception.send-message', 'faunus\handlers\ExceptionEventHandler@onException');
    }
}