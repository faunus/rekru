<?php
Route::get('/',
	['uses' => 'WebController@landingPage', 'as' => 'rekru.index']);
Route::get('vacante',
	['uses' => 'WebController@seleccionarVacante', 'as' => 'rekru.action']);
Route::post('vacante',
	['uses' => 'JobController@getJob', 'as' => 'rekru.action.post']);
Route::get('registro',
	['uses' => 'WebController@registroEmpresa', 'as' => 'rekru.signin']);
Route::get('user/{user_id}',
	['uses' => 'UserController@dashboard', 'as' => 'user.dashboard'])
	->where(['user_id' => '[A-Za-z0-9-]+']);
Route::post('user',
	['uses' => 'UserController@registroEmpresa', 'as' => 'user.dashboard.post']);
Route::get('comprar/{cv_id}',
	['uses' => 'ShopController@cv', 'as' => 'shop.cv'])
	->where(['cv_id' => '[A-Zaz0-9]+']);

// queues
Route::post('queue/push', function ()
{
	return Queue::marshall();
});

Event::subscribe('faunus\handlers\UserEventHandler');

App::missing(function ( $exception )
{
	// return Response::view('mvp.error-404', array(), 404);
	return Redirect::route('rekru.index');
});