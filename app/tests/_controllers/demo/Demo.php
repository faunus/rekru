<?php

class Demo extends TestCase {

	public function testLandingPage()
	{
		$this->action('GET', 'WebController@landingPage');
	}

	public function testSeleccionarVacante()
	{
	    $this->action('GET', 'WebController@seleccionarVacante');
	}

	public function testEnviarVacante()
	{
	    $this->action('POST', 'JobController@getJob');
	}

	public function testComprarCV()
	{
	    $this->action('GET', 'ShopController@cv' , ['cv_id' => '999']);
	}

	public function testRegistroEmpresa()
	{
	    $this->action('GET', 'WebController@registroEmpresa');
	}
}