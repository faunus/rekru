<?php

function slug_username_from_email($email)
{
	// explode just the username part
	$username = explode("@", $email, -1);
	return Str::slug($username[0], '-');
}