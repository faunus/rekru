@extends('_master.base-demo')

@section('navbar')
	<nav class="navbar navbar-default" role="navigation">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<a class="navbar-brand" href="{{ URL::route('rekru.index') }}">Rekru</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<!-- <div class="collapse navbar-collapse navbar-ex1-collapse"> -->
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="{{ URL::route('rekru.action') }}">Subir vacante</a></li>
				<li><a href="#">Créditos en tu cuenta: $0</a></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</nav>
@stop

@section('container') 
	<div class="wrapper">
		@include('web.includes.exception-view')
		@include('web.includes.success-view')
		@include('user.includes.geeks')
		@include('user.includes.sign-up')
	</div>
 @stop 