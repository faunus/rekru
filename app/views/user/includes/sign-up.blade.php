<h3 class="centrar-texto">
	¿Deseas ser notificado cuando Rekru este listo?
</h3>
<div class="row">
	{{ Form::open(['route' => 'user.dashboard.post', 'class' => 'form-horizontal', 'role' => 'form']) }}
	<!-- name -->
	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-8">
			{{ $errors->first('name', '<span style="color: yellow;">:message</span>') }}
		</div>
		<div class="col-sm-offset-1 col-sm-2">
			{{ Form::label('name', 'Nombre', ['class' => 'control-label']); }}
		</div>
		<div class="col-sm-8">
			{{ Form::input('text', 'name', null, ['class' => 'form-control input-lg']) }}
		</div>
	</div>
	<!-- email -->
	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-8">
			{{ $errors->first('email', '<span style="color: yellow;">:message</span>') }}
		</div>
		<div class="col-sm-offset-1 col-sm-2">
			{{ Form::label('email', 'Correo para recibir', ['class' => 'control-label']); }}
		</div>
		<div class="col-sm-8">
			{{ Form::input('email', 'email', null, ['class' => 'form-control input-lg', 'placeholder' => 'me@enterprise.com']) }}
		</div>
	</div>
	<!-- enterprise -->
	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-8">
			{{ $errors->first('enterprise', '<span style="color: yellow;">:message</span>') }}
		</div>
		<div class="col-sm-offset-1 col-sm-2">
			{{ Form::label('enterprise', 'Empresa', ['class' => 'control-label']); }}
		</div>
		<div class="col-sm-8">
			{{ Form::input('text', 'enterprise', null, ['class' => 'form-control input-lg']) }}
		</div>
	</div>
	<!-- phone -->
	<div class="form-group">
		<div class="col-sm-offset-3 col-sm-8">
			{{ $errors->first('phone', '<span style="color: yellow;">:message</span>') }}
		</div>
		<div class="col-sm-offset-1 col-sm-2">
			{{ Form::label('phone', 'Teléfono', ['class' => 'control-label']); }}
		</div>
		<div class="col-sm-8">
			{{ Form::input('text', 'phone', null, ['class' => 'form-control input-lg']) }}
		</div>
	</div>
	<div class="form-group">
		<div class="text-center">
			{{ Form::submit('Notificarme', ['class' => 'btn btn-success btn-lg']) }}	
		</div>	
	</div>
	{{ Form::close() }}
</div>