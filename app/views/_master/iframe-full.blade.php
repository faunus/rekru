<!doctype html>
<html lang="es">
<head>
<link href="" rel="publisher" />
<meta charset="UTF-8">
<title>{{ $page->title }}</title>

<style type="text/css">
	body, html {
		margin: 0;
		padding: 0;
		height: 100%;
		overflow: hidden;
	}
	
	#content {
		position: absolute;
		left: 0;
		right: 0;
		bottom: 0;
		top: 0px;
		background: transparent;
		height: expression(document.body.clientHeight-90);
		/*margin-top: 60px;*/
	}
</style>

</head>
<body>
<!-- iframe from hell! for you (; shh!  -->
<div id="content">
	<iframe src="{{ $page->url }}" width="100%" height="100%"></iframe>
</div>
<!-- le scripts -->
@section('scripts')
	@include('_javascripts/google-analytics')
@show
<!-- bye scripts -->
</body>
</html>