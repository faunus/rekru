<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Rekru - Head Hunter Online</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<!-- Latest compiled and minified CSS -->
	<!-- <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css"> -->
	<link rel="stylesheet" href="/test/bootstrap.min.css">

	<!-- font -->
	<link href='http://fonts.googleapis.com/css?family=Raleway:500,300,200,600,900' rel='stylesheet' type='text/css'>

	<!-- Latest compiled and minified JavaScript -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.min.js"></script>
	<style>
		body {
			background: rgba(15,148,192,1);
			background-image: url('/backgrounds/pattern.png');
			background-repeat: repeat;
			font-family: "Raleway", Arial, sans-serif;
			font-size: 18px;
			line-height: 29px;
			font-weight: 200;
		}

		.navbar {
			background: black;
			background-image: url('/backgrounds/pattern2.png');
			background-repeat: repeat;
			border: 1px solid black;
		}

		.navbar-default .navbar-brand {
			color: white;
		}

		.navbar-default .navbar-brand:hover {
			color: white;
		}

		.navbar-default .navbar-nav>li>a {
			color: white;
		}
		.navbar-default .navbar-nav>li>a:hover {
			color: white;
		}

		.centrar-imagen{
		    display: block;
		    margin-left: auto;
		    margin-right: auto;
		}

		.centrar-texto {
		    text-align: center;
		}

		.centrar-tabla {
			margin: auto;
		}
	
		.wrapper {
			margin: 0 18px;
		}

		label {
			color: white;
			font-size: 18px;
			line-height: 29px;
			font-family: "Raleway", Arial, sans-serif;
			font-weight: 500;
		}

		h1 {
			color: white;
			font-size: 26px;
			line-height: 42px;
			font-family: "Raleway", Arial, sans-serif;
			font-weight: 200; 
		}

		h2{
			color: white;
		    font-size: 26px;
		    line-height: 36px;
		    font-family: "Raleway", Arial, sans-serif;
		    font-weight: 500;
		}
		h3{
			color: white;
			font-size: 26px;
			line-height: 36px;
			font-family: "Raleway", Arial, sans-serif;
			font-weight: 300;
		}

		.geek {
			border-radius: 6px;
			background-color: rgb(15,148,192);
			padding: 18px 0px;
			margin-top: 18px;
			color: white;
		}
		/** { box-shadow: inset 0 0 5px white; }*/
	</style>
</head>
<body>
	@section('navbar')
		<nav class="navbar navbar-default" role="navigation">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<a class="navbar-brand" href="{{ URL::route('rekru.index') }}">Rekru</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav">
					<li><a href="{{ URL::route('rekru.action') }}">Subir vacante</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</nav>
	@show
	<!-- navbar -->
	<!-- container-->
	@yield('container')
	<!-- footer-->
	<!-- <nav class="navbar navbar-fixed-bottom">
		el footer
	</nav> -->
</body>
@section('scripts')
	@include('_javascripts/google-analytics')
@show
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
</html>