@if (Session::has('exception'))
	<div class="alert alert-danger text-center">
		{{ Session::get('exception') }}
	</div>
@endif