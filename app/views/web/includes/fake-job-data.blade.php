<h1 class="centrar-texto">Demo</h1>

<h2 class="centrar-texto">Detalles de la vacante</h2>
<br>
<div class="row">
	<div class="form-group">
		<div class="col-sm-offset-1 col-sm-2">
			<label class="hidden-xs control-label" for="title">Nombre del Puesto: </label>
			<label class="visible-xs control-label centrar-texto" for="title">Nombre del Puesto</label>
		</div>
		<div class="col-sm-8">
			<input type="text" id="title" class="form-control input-lg" placeholder="Programador Java en Guadalajara" disabled>
		</div>
	</div>
</div> <!-- bye title -->
<br>
<div class="row">
	<div class="form-group">
		<div class="col-sm-offset-1 col-sm-2">
			<label class="hidden-xs control-label" for="region">Ciudad:</label>
			<label class="visible-xs control-label centrar-texto" for="region">Ciudad</label>
		</div>
		<div class="col-sm-8">
			<input type="text" id="title" class="form-control input-lg" placeholder="Guadalajara" disabled>
		</div>
	</div>			
</div> <!-- bye region -->
<br>
<div class="row">
	<div class="form-group">
		<div class="col-sm-offset-1 col-sm-2">
			<label class="hidden-xs control-label" for="money">Sueldo Ofrecido:</label>
			<label class="visible-xs control-label centrar-texto" for="money">Sueldo Ofrecido</label>
		</div>
		<div class="col-sm-8">
			<input type="number" id="title" class="form-control input-lg" placeholder="$30,000.-  a $50,000.-  más prestaciones" disabled>
		</div>
	</div>
</div> <!-- bye money -->
<br>
<div class="row">
	<div class="form-group">
		<div class="col-sm-offset-1 col-sm-2">
			<label class="hidden-xs control-label" for="kind">Perfil de puesto:</label>
			<label class="visible-xs control-label centrar-texto" for="kind">Perfil de puesto</label>
		</div>
		<div class="col-sm-8">
			<input type="number" id="title" class="form-control input-lg" placeholder="Programador Java con experiencia de 5 a 10 años, Inglés avanzado" disabled>
		</div>
	</div>
</div> <!-- bye kind -->
<br>