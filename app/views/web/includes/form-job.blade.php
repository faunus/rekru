{{ Form::open(['action' => 'JobController@getJob', 'class' => 'form', 'role' => 'form']) }}
<div class="row">
	<!-- email -->
	<div class="form-group">
		<div class="centrar-texto col-sm-offset-3 col-sm-8">
			{{ $errors->first('email', '<span style="color: yellow;">:message</span>') }}
		</div>
		<div class="col-sm-offset-1 col-sm-2">
			{{ Form::label('email', 'Correo para recibir recomendaciones:', ['class' => 'hidden-xs control-label']); }}
		</div>
		<div class="col-sm-offset-1 col-sm-2">
			{{ Form::label('email', 'Correo para recibir recomendaciones', ['class' => 'visible-xs control-label centrar-texto']); }}
		</div>
		<div class="col-sm-8">
			{{ Form::input('email', 'email', null, ['class' => 'form-control input-lg', 'placeholder' => 'me@enterprise.com']) }}
		</div>
	</div>
</div> <!-- bye email -->
<br>
<div class="form-group">
	<div class="centrar-texto">
		{{ Form::submit('Agregar Vacante', ['class' => 'btn btn-success btn-lg']) }}	
	</div>	
</div>
{{ Form::close() }}