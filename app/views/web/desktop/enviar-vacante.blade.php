@extends('_master.base-demo')
@section('container') 
	<div class="wrapper">
		@include('web.includes.exception-view')
		@include('web.includes.success-view')
		@include('web.includes.fake-job-data')
		@include('web.includes.form-job')
	</div>
 @stop 