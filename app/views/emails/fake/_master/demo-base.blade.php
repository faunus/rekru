<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="initial-scale=1.0">
	<!-- So that mobile webkit will display zoomed in -->
	<meta name="format-detection" content="telephone=no">
	<!-- disable auto telephone linking in iOS -->

	<title>Empleos TI :: Correo</title>
	<style type="text/css">
		/* Resets: see reset.css for details */
		.ReadMsgBody {
			width: 100%;
			background-color: #ebebeb;
		}

		.ExternalClass {
			width: 100%;
			background-color: #ebebeb;
		}

		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
			line-height: 100%;
		}

		body {
			-webkit-text-size-adjust: none;
			-ms-text-size-adjust: none;
		}

		body {
			margin: 0;
			padding: 0;
		}

		table {
			border-spacing: 0;
		}

		table td {
			border-collapse: collapse;
		}

		.yshortcuts a {
			border-bottom: none !important;
		}

		/* Constrain email width for small screens */
		@media screen and (max-width: 600px) {
			table[class="container"] {
				width: 95% !important;
			}
		}

		/* Give content more room on mobile */
		@media screen and (max-width: 480px) {
			td[class="container-padding"] {
				padding-left: 12px !important;
				padding-right: 12px !important;
			}
		}
	</style>
</head>
<body style="margin:0; padding:10px 0;" bgcolor="#ebebeb" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	@yield('container')
</body>
</html>

