<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<p>
			Dulce Maria Guzman, te ha recomendado un curriculum para tu vacante <b>=Programador Java=</b>.
		</p>
		
		<p>
			<i>Ver CV adjunto</i>
		</p>

		<p>
			<b>Sus comentarios: </b>
		</p>

		<p>
			Actualmente trabaja 100% en Java, con IBM en un proyecto de editores.
			Los proyectos involucran equipo en otros paises.
			Estudio la Maestria en la UDLA becado un 90% por Conacyt y UDLA.
			Su tesis fue con Ruby y lo aprendió autodidacta.
			Actualmente 100% nomina y Gana $29,000 netos, Aprox $45 brutos.
		</p>

		<a href="{{ URL::route('user.dashboard', ['user_id' => '$username'], true) }}">
			Ir a dashboard para ver mis candidatos
		</a>
	</body>
</html>