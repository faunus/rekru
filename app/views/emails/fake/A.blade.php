@extends('emails/fake/_master/demo-base')

@section('container')
<!-- 100% wrapper (grey background) -->
<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#ebebeb">
    <tr>
        <td align="center" valign="top" bgcolor="#ebebeb" style="background-color: #ebebeb;">

            <!-- 600px container (white background) -->
            <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" bgcolor="#ffffff">
                <tr>
                    <td class="container-padding" bgcolor="#ffffff" style="background-color: #f5f5f5; padding-left: 30px; padding-right: 30px; font-size: 14px; line-height: 20px; font-family: Helvetica, sans-serif; color: #333;">
                        <br>
                        <!-- ### BEGIN CONTENT ### -->
                        <div style="text-align: center; font-weight: bold; font-size: 32px; line-height: 24px; color: #336699">
                            Has recibido un candidato
                        </div>
                        <br>
                        <div style="text-align: center; font-weight: bold; font-size: 18px; line-height: 24px; color: #336699">
                            Vacante: Programador Java en Guadalajara 
                        </div>

                        <div style="text-align: center;">
                            <i style="text-align: center;">~Ver CV adjunto~</i>
                        </div>
                        <br>
                        <div style="font-size: 20px; line-height: 24px;">
                            <strong>Comentarios del Reclutador:</strong>
                            <br>
                            <div>
                                Actualmente en TCS desde hace 2 años. Coordina 8 aplicaciones siendo su rol 80% técnico. Tiene 3 certificaciones, de cuando fue instructor Java. Actual $60,000.- brutos más prestaciones por nomina. Me parece muy alto este dato recomiendo verificar.
                            </div> 
                        </div><!-- bye comentarios -->
                        <br>
                        <div style="font-size: 18px; line-height: 24px;">
                            <strong>Estadísticas del Reclutador:</strong>
                            <br>
                            <div>
                                <ul>
                                    <li>70% Confiabilidad CVs</li>
                                    <li>70% Efectividad</li>
                                </ul>
                            </div>
                        </div><!-- bye estdisticas -->
                        <br>
                        <div style="text-align: center; font-size: 24px; background: #336699; padding-top: 12px; padding-right: 10px; padding-bottom: 12px; padding-left: 10px; -webkit-border-radius: 4px; -moz-border-radius: 4px; border-radius: 4px; color: #fff; font-weight: bold; text-decoration: none; font-family: Helvetica, Arial, sans-serif;">
                            <a style="font-size: 24px; color: #fff; text-decoration: none;" href="{{ URL::route('user.dashboard', ['user_id' => $username], true) }}">Ir a mi panel de candidatos</a>
                        </div><!-- bye button -->
                        <br>
                    </td><!-- ### END CONTENT ### -->
                </tr>
                <!-- foot -->
                @include('emails/fake/_master/includes/footer')
            </table>
            <!--/600px container -->
        </td>
    </tr>
</table>
<!--/100% wrapper-->
<br>
<br>
@stop