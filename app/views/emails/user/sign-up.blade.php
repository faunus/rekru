@extends('emails/fake/_master/demo-base')

@section('container')
<!-- 100% wrapper (grey background) -->
<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#ebebeb">
    <tr>
        <td align="center" valign="top" bgcolor="#ebebeb" style="background-color: #ebebeb;">

            <!-- 600px container (white background) -->
            <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" bgcolor="#ffffff">
                <tr>
                    <td class="container-padding" bgcolor="#ffffff" style="background-color: #f5f5f5; padding-left: 30px; padding-right: 30px; font-size: 14px; line-height: 20px; font-family: Helvetica, sans-serif; color: #333;">
                        <br>
                        <br>
                        <br>
                        <!-- ### BEGIN CONTENT ### -->
                        <div style="text-align: center; font-weight: bold; font-size: 32px; line-height: 24px; color: #336699">
                            Rekru ~ Tu Head Hunter Online
                        </div>
                        <br>
                        <br>
                        <div style="text-align: center; font-weight: bold; font-size: 18px; line-height: 24px; color: #336699">
                            Nosotros te avisamos cuando estemos listos (:
                        </div>
                        <br>
                        <br>
                        <br>
                    </td><!-- ### END CONTENT ### -->
                </tr>
                <!-- foot -->
                @include('emails/fake/_master/includes/footer')
            </table>
            <!--/600px container -->
        </td>
    </tr>
</table>
<!--/100% wrapper-->
<br>
<br>
@stop