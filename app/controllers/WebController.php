<?php

class WebController extends BaseController {

	/**
	 * Display the index page.
	 *
	 * @return Response
	 */
	public function landingPage()
	{
		$page = new stdClass();
		$page->title = 'Rekru - Head Hunter Online';
		$page->url = 'http://unbouncepages.com/rekru/';

		$data = ['page' => $page];
		return View::make('web/desktop/landing-page', $data);
	}

	public function seleccionarVacante()
	{
		return View::make('web/desktop/enviar-vacante');
	}

	public function registroEmpresa()
	{
		return 'registroEmpresa';
	}

}
