<?php

class JobController extends BaseController {

	private $email;
	function __construct(\Email $email) {
		$this->email = $email;
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getJob()
	{
		$v = new faunus\services\validators\Mail;

		if($v->passes()) {

		  	// verify email is not already at db
			$is_there = $this->email->where('email', '=', Input::get('email'))->count();

			if ($is_there == 0) {
				try 
				{
		  		    // save email at db
					$this->email->create(Input::all());
				}
				catch (Exception $e)
				{
		  			// Event::fire('exception.send-message', $data);

					Session::flash( 'exception', 'Disculpa hubo un error, Intenta de nuevo (:' );
					return Redirect::route('rekru.action');
				}
			}

	 		// send email with fake cv's
			$subject = 'Rekru | Candidato para "Programador Java en Guadalajara"';
			$view = 'emails/fake/B';
			$path = app_path() . '/lib/rekru/resumes/B.pdf';

			$user = new stdClass();
			$user->email = Input::get('email');
			$user->attach = app_path() . '/lib/rekru/resumes/B.pdf';
			$user->username = slug_username_from_email(Input::get('email'));

	 		// $vars = ['username' => slug_username_from_email(Input::get('email'))];

	 		// Session::put('mvp-email', Input::get('email'));

			$data = [$user, $subject, $view];
			Event::fire('user.send-fake-data', $data);

	 		// send email 2 with fake cv's
			$view = 'emails/fake/A';
			$user->attach = app_path() . '/lib/rekru/resumes/A.pdf';

			$data = [$user, $subject, $view];
			Event::fire('user.send-fake-data', $data);

			Session::flash('success', 'En unos momentos recibirás currículums recomendados (:');
			return Redirect::route('rekru.action');
		}

			return Redirect::back()->withInput()->withErrors($v->getErrors());
	}
}
