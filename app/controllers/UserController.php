<?php

class UserController extends BaseController {

	private $signup;

	function __construct(\Signup $signup) {
		$this->signup = $signup;
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function dashboard($user)
	{
		// return $user;
		return View::make('user/desktop/dashboard');
	}

	public function registroEmpresa()
	{
			$v = new faunus\services\validators\Signup;

			if($v->passes()) {

			  	// verify email is not already at db
				$is_there = $this->signup->where('email', '=', Input::get('email'))->count();

				if ($is_there == 0) {
					try 
					{
			  		    // save email at db
						$this->signup->create(Input::all());
					}
					catch (Exception $e)
					{
			  			// Event::fire('exception.send-message', $data);
						Session::flash( 'exception', 'Disculpa hubo un error, Intenta de nuevo (:' );
						return Redirect::route('rekru.action');
					}
				}

		 		// send email
				$user = new stdClass();
				$user->email = Input::get('email');
				$user->name = Input::get('name');

				$data = [$user];
				Event::fire('user.signUp', $data);

				return Redirect::route('rekru.index');
			}
			
			return Redirect::back()->withInput()->withErrors($v->getErrors());
	}
}
